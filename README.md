ansible-role-packages
=========

[![Build Status](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/pipelines)

Ansible role to install packages required by roles

Using this role
--------------

The role needs to be loaded dynamically during the plays and the dictionary of parameters to pass to the role.

Role Variables
--------------

This role will need to be provided with a dictionary of variables that have this structure.

```
namespace_packages: # Namespace variable of the role
  yum: # name of package manager to use
    - package
      # Package to be installed, include version if 
      # required 
  apt:
    - package
```

Example Playbook
----------------

Example import:
```
...
- import_role:
    name: ansible-role-packages
  vars:
    packages: "{{ namespace_packages }}"
...
```
Example Vars:

```
test_packages:
  yum:
    - '{{ yum packages }}
  pip:
    - {{ pippy_packages }}
    update_cache: yes
```
License
-------

MIT

Author Information
------------------
The Development Range Engineering, Architecture, and Modernization (DREAM) Team
